angular.module('myApp', ['ngRoute', 'ngResource', 'myApp.schedule'])

.config(['$locationProvider','$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider
    
    .when('/day-schedule', {
        templateUrl: 'day-schedule.html',
    })

    .otherwise({redirectTo: '/schedule'});
}])

.filter('filterMonth', function () {
    return function (items, letter) {
        var filtered = [];
        var letterMatch = new RegExp(letter, 'i');
        for (var i = 0; i < items.length; i++) {
          var item = items[i];
          if (letterMatch.test(item.name.substring(0)) || item.m.includes(letter)) {
            filtered.push(item);
          }
        }
        return filtered;
      };
})

.filter('filterPeople', function () {
    return function (items, letter) {
        var filtered = [];
        var letterMatch = new RegExp(letter, 'i');
        for (var i = 0; i < (items || {}).length; i++) {
            var item = items[i];
            if (letterMatch.test(item.person.substring(0))){
                if(filtered.length==0) filtered.push(item);
                else {
                    var x = 0;
                    for (var j = 0; j < filtered.length; j++) {
                        if ((item.person).localeCompare(filtered[j].person)==0) x=1;
                    }
                    if(x==0) filtered.push(item);
                }
            }
        }
        return filtered;
      };
})

.factory('MyService', ['$http', function($http) {

    var urlBase = 'http://localhost:3000/tasks';
    var MyService = {};

    MyService.getTasks = function () {
        return $http.get(urlBase);
    };

    MyService.getTask = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    MyService.addTask = function (task) {
        return $http.post(urlBase, task);
    };

    MyService.updateTask = function (task) {
        return $http.put(urlBase + '/' + task.id, task)
    };

    MyService.deleteTask = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

    return MyService;
}])

.component('task', {
    templateUrl: 'task.html',
    bindings: {
        name: '<',
        category: '<',
        date: '<',
        time: '<',
        person: '<'
    }
})

.component('taskList', {
    templateUrl: 'taskList.html',

    controller: function ($scope, $http, MyService) {

        $scope.tasks;
        $scope.task;
        $scope.name;
        $scope.category;
        $scope.date;
        $scope.time;
        $scope.person;

        this.$onInit = function() {
            var $scope = this;
            MyService.getTasks()
            .then(function (response) {
                $scope.tasks = response.data;
                this.name = '';
                this.category = '';
                this.date = '';
                this.time = '';
                this.person = '';
            }, function (error) {
                $scope.status = 'Page not found ' + error.message;
            });
            $scope.task = {};
        }

        this.addTask = function () {
            var new_task = {
                name: this.task.name,
                category: this.task.category,
                date: this.task.date,
                time: this.task.time,
                person: this.task.person};
            MyService.addTask(new_task)
                .then(function (response) {
                    this.tasks.push(new_task);
                }, function(error) {
                    $scope.status = 'Page not found ' + error.message;
                });
        };

        this.getDetails = function (id) {
            MyService.getTask(id)
            .then(function (response) {
                $scope.task = response.data;
            }, function (error) {
                $scope.status = 'Page not found ' + error.message;
            });
        };

        this.startEditing = function (id) {
            MyService.getTask(id)
            .then(function (response) {
                $scope.task = response.data;
                $scope.name = response.data.name;
                $scope.category = response.data.category;
                $scope.date = response.data.date;
                $scope.time = response.data.time;
                $scope.person = response.data.person;
                console.log($scope.task);
            }, function (error) {
                $scope.status = 'Page not found ' + error.message;
            });
        };

        this.editTask = function (id) {
            for (var i = 0; i < ($scope.tasks || {}).length; i++) {
                var t = $scope.tasks[i];
                if (t.id === id) {
                    $scope.task = t;
                    
                    break;
                }
            }
            console.log($scope.task.id);
            MyService.updateTask($scope.task)
              .then(function (response) {
              }, function (error) {
                  $scope.status = 'Page not found ' + error.message;
              });
        };

        this.deleteTask = function (id) {
            for (var i = 0; i < (this.tasks || {}).length; i++) {
                var task = this.tasks[i];
                if (task.id === id) {
                    this.tasks.splice(i, 1);
                    break;
                }
            }
            MyService.deleteTask(id)
            .then(function (response) {
                
            }, function (error) {
                $scope.status = 'Page not found ' + error.message;
            });
        };

        this.months = [
            {m:'01', name:'Styczeń', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'02', name:'Luty', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28']},
            {m:'03', name:'Marzec', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'04', name:'Kwiecień', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']},
            {m:'05', name:'Maj', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'06', name:'Czerwiec', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']},
            {m:'07', name:'Lipiec', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'08', name:'Sierpień', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'09', name:'Wrzesień', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']},
            {m:'10', name:'Październik', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'11', name:'Listopad', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']},
            {m:'12', name:'Grudzień', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']}
        ];

        this.taskActive = {
            "font-weight" : "bold"
        };

        this.taskInactive = {
            "font-weight" : "normal"
        };
    }
})

.component('dayTaskList', {
    templateUrl: 'dayTaskList.html',

    controller: function ($scope, $http, MyService) {

        $scope.tasks;
        $scope.task;
        $scope.name;
        $scope.category;
        $scope.date;
        $scope.time;
        $scope.person;

        this.$onInit = function() {
            var $scope = this;
            MyService.getTasks()
            .then(function (response) {
                $scope.tasks = response.data;
                this.name = '';
                this.category = '';
                this.date = '';
                this.time = '';
                this.person = '';
            }, function (error) {
                $scope.status = 'Page not found ' + error.message;
            });
            $scope.task = {};
        }

        this.addTask = function () {
            var new_task = {
                name: this.task.name,
                category: this.task.category,
                date: this.task.date,
                time: this.task.time,
                person: this.task.person};
            MyService.addTask(new_task)
                .then(function (response) {
                    this.tasks.push(new_task);
                }, function(error) {
                    $scope.status = 'Page not found ' + error.message;
                });
        };

        this.getDetails = function (id) {
            MyService.getTask(id)
            .then(function (response) {
                $scope.task = response.data;
            }, function (error) {
                $scope.status = 'Page not found ' + error.message;
            });
        };

        this.startEditing = function (id) {
            MyService.getTask(id)
            .then(function (response) {
                $scope.task = response.data;
                $scope.name = response.data.name;
                $scope.category = response.data.category;
                $scope.date = response.data.date;
                $scope.time = response.data.time;
                $scope.person = response.data.person;
                console.log($scope.task);
            }, function (error) {
                $scope.status = 'Page not found ' + error.message;
            });
        };

        this.editTask = function (id) {
            for (var i = 0; i < ($scope.tasks || {}).length; i++) {
                var t = $scope.tasks[i];
                if (t.id === id) {
                    $scope.task = t;
                    
                    break;
                }
            }
            console.log($scope.task.id);
            MyService.updateTask($scope.task)
              .then(function (response) {
              }, function (error) {
                  $scope.status = 'Page not found ' + error.message;
              });
        };

        this.deleteTask = function (id) {
            for (var i = 0; i < (this.tasks || {}).length; i++) {
                var task = this.tasks[i];
                if (task.id === id) {
                    this.tasks.splice(i, 1);
                    break;
                }
            }
            MyService.deleteTask(id)
            .then(function (response) {
                
            }, function (error) {
                $scope.status = 'Page not found ' + error.message;
            });
        };

        this.months = [
            {m:'01', name:'Styczeń', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'02', name:'Luty', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28']},
            {m:'03', name:'Marzec', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'04', name:'Kwiecień', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']},
            {m:'05', name:'Maj', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'06', name:'Czerwiec', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']},
            {m:'07', name:'Lipiec', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'08', name:'Sierpień', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'09', name:'Wrzesień', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']},
            {m:'10', name:'Październik', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']},
            {m:'11', name:'Listopad', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']},
            {m:'12', name:'Grudzień', d: ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']}
        ];

        this.taskActive = {
            "font-weight" : "bold"
        };

        this.taskInactive = {
            "font-weight" : "normal"
        };
    }
})

.directive('category', function() {
    return {
      restrict: 'E',
      template: function(elem, attr) {
        return '<i class="fa fa-' + attr.type + '"></i>';
      }
    };
})

.directive('customDateDirective', function() {
    var DATE_REGEX = /^(?:(?:31(-)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(-)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(-)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(-)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/i;
    return {
        require: 'ngModel',
        link: function(scope, element, attr, mCtrl) {
            function dateValidation(value) {
                if (DATE_REGEX.test(value)) {
                    mCtrl.$setValidity('date', true);
                    console.log('ok');
                } else {
                    mCtrl.$setValidity('date', false);
                    console.log('fail');
                }
                return value;
            }
            mCtrl.$parsers.push(dateValidation);
        }
    };
})

.directive('customTimeDirective', function() {
    var TIME_REGEX = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/i;
    return {
        require: 'ngModel',
        link: function(scope, element, attr, mCtrl) {
            function timeValidation(value) {
                if (TIME_REGEX.test(value)) {
                    mCtrl.$setValidity('time', true);
                    console.log('ok');
                } else {
                    mCtrl.$setValidity('time', false);
                    console.log('fail');
                }
                return value;
            }
            mCtrl.$parsers.push(timeValidation);
        }
    };
});