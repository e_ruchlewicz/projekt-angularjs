const express = require('express');
var cors = require('cors');

const app = express();

app.use(express.json());
app.use(cors());

const tasks = [
    { id: 1, name: 'Biometria wykład', category: "Studia", date: "07-05-2018", time: "14:15", person: "Ewelina"},
    { id: 2, name: 'AIOoK ps', category: "Studia", date: "07-05-2018", time: "16:15", person: "Michał"},
    { id: 3, name: 'Angielski', category: "Studia", date: "08-05-2018", time: "10:15", person: "Marcin"},
    { id: 4, name: 'Egzamin BSK', category: "Studia", date: "25-06-2018", time: "10:15", person: "Ewelina"},
    { id: 5, name: 'Praca', category: "Praca", date: "21-05-2018", time: "8:00", person: "Kamil"},
    { id: 6, name: 'Okulista', category: "Zdrowie", date: "23-05-2018", time: "16:15", person: "Marcin"},
    { id: 7, name: 'Siłownia', category: "Sport", date: "10-05-2018", time: "10:30", person: "Michał"},
    { id: 8, name: 'Mycie okien', category: "Dom", date: "16-05-2018", time: "12:00", person: "Ewelina" },
    { id: 9, name: 'Głaskanie kota', category: "Dom", date: "16-01-2018", time: "12:00", person: "Ewelina" },
    { id: 10, name: 'Budowanie czołgu', category: "Dom", date: "18-01-2018", time: "12:00", person: "Marcin" },
    { id: 11, name: 'Programowanie', category: "Studia", date: "19-01-2018", time: "12:00", person: "Marcin" },
    { id: 12, name: 'Programowanie', category: "Studia", date: "20-01-2018", time: "15:00", person: "Marcin" },
    { id: 13, name: 'Programowanie', category: "Studia", date: "21-01-2018", time: "12:00", person: "Marcin" },
    { id: 14, name: 'Jazda na rowerze', category: "Studia", date: "21-01-2018", time: "12:00", person: "Ewelina" },
    { id: 15, name: 'Czesanie kota', category: "Dom", date: "16-01-2018", time: "13:00", person: "Ewelina" },
    { id: 16, name: 'Głaskanie kota x2', category: "Dom", date: "16-01-2018", time: "14:00", person: "Ewelina" },
    { id: 17, name: 'Jazda samochodem', category: "Zdrowie", date: "28-01-2018", time: "10:00", person: "Marcin" }
];

app.get('/tasks', (req, res) => {
    res.send(tasks);
});

app.post('/tasks', (req,res) => {
    if (!req.body.name || !req.body.category || !req.body.date || !req.body.time || !req.body.person) return res.status(404).send('Bad Request posting');
    const task = {
        id: tasks.length+1,
        name: req.body.name,
        category: req.body.category,
        date: req.body.date,
        time: req.body.time,
        person: req.body.person
    };
    tasks.push(task);
    res.send(task);
});

app.put('/tasks/:id', (req, res) => {
    const task = tasks.find(c => c.id === parseInt(req.params.id));
    if(!task) return res.status(404).send('Not Found');
    task.name = req.body.name;
    task.category = req.body.category;
    task.date = req.body.date;
    task.time = req.body.time;
    task.person = req.body.person;
    res.send(task);
});

app.get('/tasks/:id', (req, res) => {
    const task = tasks.find(c => c.id === parseInt(req.params.id));
    if(!task) res.status(404).send('Not Found');
    res.send(task);
});

app.delete('/tasks/:id', (req, res) => {
    const task = tasks.find(c => c.id === parseInt(req.params.id));
    if(!task) return res.status(404).send('Not Found');
    const index = tasks.indexOf(task);
    tasks.splice(index, 1);
    res.send(task);
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening on port ${port}`));

